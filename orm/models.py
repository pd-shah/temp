from django.db import models


# Create your models here.
class Supplier(models.Model):
    name = models.CharField(max_length=256)
    city = models.CharField(max_length=256)


class Product(models.Model):
    name = models.CharField(max_length=256)
    color = models.CharField(max_length=256)


class SP(models.Model):
    supplier = models.ForeignKey("Supplier", on_delete=models.CASCADE)
    product = models.ForeignKey("Product", on_delete=models.CASCADE)
    count = models.IntegerField(default=1)
